package io.hmpl.utils;

import io.hmpl.persistence.jpa.entity.Episode;
import io.hmpl.persistence.jpa.entity.Series;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.Json;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MemoryAlphaCrawler {

	private static final String tosS01 = "http://memory-alpha.wikia.com/wiki/TOS_Season_1";
	private static final String tosS02 = "http://memory-alpha.wikia.com/wiki/TOS_Season_2";
	private static final String tosS03 = "http://memory-alpha.wikia.com/wiki/TOS_Season_3";

	private static final List<String> tosURLs = Arrays.asList(tosS01, tosS02, tosS03);

	private static final String tngS01 = "http://memory-alpha.wikia.com/wiki/TNG_Season_1";
	private static final String tngS02 = "http://memory-alpha.wikia.com/wiki/TNG_Season_2";
	private static final String tngS03 = "http://memory-alpha.wikia.com/wiki/TNG_Season_3";
	private static final String tngS04 = "http://memory-alpha.wikia.com/wiki/TNG_Season_4";
	private static final String tngS05 = "http://memory-alpha.wikia.com/wiki/TNG_Season_5";
	private static final String tngS06 = "http://memory-alpha.wikia.com/wiki/TNG_Season_6";
	private static final String tngS07 = "http://memory-alpha.wikia.com/wiki/TNG_Season_7";

	private static final List<String> tngUrls = Arrays.asList(tngS01, tngS02, tngS03, tngS04, tngS05, tngS06, tngS07);

	private static final String ds9S01 = "http://memory-alpha.wikia.com/wiki/DS9_Season_1";
	private static final String ds9S02 = "http://memory-alpha.wikia.com/wiki/DS9_Season_2";
	private static final String ds9S03 = "http://memory-alpha.wikia.com/wiki/DS9_Season_3";
	private static final String ds9S04 = "http://memory-alpha.wikia.com/wiki/DS9_Season_4";
	private static final String ds9S05 = "http://memory-alpha.wikia.com/wiki/DS9_Season_5";
	private static final String ds9S06 = "http://memory-alpha.wikia.com/wiki/DS9_Season_6";
	private static final String ds9S07 = "http://memory-alpha.wikia.com/wiki/DS9_Season_7";

	private static final List<String> ds9Urls = Arrays.asList(ds9S01,ds9S02, ds9S03, ds9S04, ds9S06, ds9S05, ds9S06, ds9S07);

	private static final String voyS01 = "http://memory-alpha.wikia.com/wiki/VOY_Season_1";
	private static final String voyS02 = "http://memory-alpha.wikia.com/wiki/VOY_Season_2";
	private static final String voyS03 = "http://memory-alpha.wikia.com/wiki/VOY_Season_3";
	private static final String voyS04 = "http://memory-alpha.wikia.com/wiki/VOY_Season_4";
	private static final String voyS05 = "http://memory-alpha.wikia.com/wiki/VOY_Season_5";
	private static final String voyS06 = "http://memory-alpha.wikia.com/wiki/VOY_Season_6";
	private static final String voyS07 = "http://memory-alpha.wikia.com/wiki/VOY_Season_7";

	private static final List<String> voyUrls = Arrays.asList(voyS01, voyS02, voyS03, voyS04, voyS05, voyS06, voyS07);

	private static final String entS01 = "http://memory-alpha.wikia.com/wiki/ENT_Season_1";
	private static final String entS02 = "http://memory-alpha.wikia.com/wiki/ENT_Season_2";
	private static final String entS03 = "http://memory-alpha.wikia.com/wiki/ENT_Season_3";
	private static final String entS04 = "http://memory-alpha.wikia.com/wiki/ENT_Season_4";

	private static final List<String> entUrls = Arrays.asList(entS01, entS02, entS03, entS04);

	private static final String dscS01 = "http://memory-alpha.wikia.com/wiki/DIS_Season_1";

	private static final List<String> dscUrls = Arrays.asList(dscS01);

	private static final Series tos = new Series("TOS", "The Original Series");
	private static final Series tng = new Series("TNG", "The Next Generation");
	private static final Series ds9 = new Series("DS9", "Deep Space 9");
	private static final Series voy = new Series("VOY", "Voyager");
	private static final Series ent = new Series("ENT", "Enterprise");
	private static final Series dsc = new Series("DSC", "Discovery");

	public static void main(String[] args) {

		List<Episode> tosEpisodes = getEpisodes(tosURLs, tos);
		List<Episode> tngEpisodes = getEpisodes(tngUrls, tng);
		List<Episode> ds9Episodes = getEpisodes(ds9Urls, ds9);
		List<Episode> voyEpisodes = getEpisodes(voyUrls, voy);
		List<Episode> entEpisodes = getEpisodes(entUrls, ent);
		List<Episode> dscEpisodes = getEpisodes(dscUrls, dsc);

		tosEpisodes = getTeaser(tosEpisodes);
		tngEpisodes = getTeaser(tngEpisodes);
		ds9Episodes = getTeaser(ds9Episodes);
		voyEpisodes = getTeaser(voyEpisodes);
		entEpisodes = getTeaser(entEpisodes);
		dscEpisodes = getTeaser(dscEpisodes);

		EntityManagerFactory entityManagerFactory =
				Persistence.createEntityManagerFactory("startrek-se");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();

		tos.setEpisodes(tosEpisodes);
		entityManager.persist(tos);

		tosEpisodes.stream().forEach(e -> entityManager.persist(e));

		tng.setEpisodes(tngEpisodes);
		entityManager.persist(tng);

		tngEpisodes.stream().forEach(entityManager::persist);

		ds9.setEpisodes(ds9Episodes);
		entityManager.persist(ds9);

		ds9Episodes.stream().forEach(entityManager::persist);

		voy.setEpisodes(voyEpisodes);
		entityManager.persist(voy);

		voyEpisodes.stream().forEach(entityManager::persist);

		ent.setEpisodes(entEpisodes);
		entityManager.persist(ent);

		entEpisodes.stream().forEach(entityManager::persist);

		dsc.setEpisodes(dscEpisodes);
		entityManager.persist(dsc);

		dscEpisodes.stream().forEach(entityManager::persist);

		entityManager.getTransaction().commit();
	}

	private static List<Episode> getEpisodes(List<String> urls, Series series) {
		return urls.stream()
				.map(MemoryAlphaCrawler::mapUrlToDoc)
				.map(doc -> {
					Elements anchors = doc.select("table.grey tbody tr a[title~=episode]");
					Stream<Episode> episodeStream = anchors.stream().map(anchor -> {
						String slug = anchor.attr("title");
						String title = slug.replace("(episode)", "");
						String ahref = anchor.attr("href");
						String seasonEpisode = anchor.parent().nextElementSibling().text();

						String[] split = seasonEpisode.split("\\D+");

						String season = split[0];
						String number = split[1];

						Episode episode = new Episode();
						episode.setName(title);
						episode.setSlug(slug);
						episode.setSeason(Integer.valueOf(season));
						episode.setNumber(Integer.valueOf(number));
						episode.setSeries(series);
						episode.setUrl("http://memory-alpha.wikia.com" + ahref);

						return episode;
					});
					return episodeStream.collect(Collectors.toList());
				}).flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	private static List<Episode> getTeaser(List<Episode> episodes) {
		return episodes.parallelStream()
				.map(episode -> {
					try {
						Document document = Jsoup.connect(episode.getUrl()).get();
						Element teaser =
								document.getElementById("Teaser").parent().parent().select("p").get(0);
						episode.setTeaser(teaser.text());
					} catch (Exception e) {
						System.err.println("Unable to fetch teaser for " + episode);
					}
					return episode;
				}).collect(Collectors.toList());
	}

	private static Document mapUrlToDoc(String url) {
		try {
			return Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
;
