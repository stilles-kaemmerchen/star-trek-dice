package io.hmpl.boundary.rest.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

public class ExampleRequest {
	@ApiModelProperty(notes = "the data")
	private final List<String> data;

	@JsonCreator
	public ExampleRequest(
			@JsonProperty("data") final List<String> data
	) {
		this.data = data;
	}

	public List<String> getData() {
		return data;
	}
}
