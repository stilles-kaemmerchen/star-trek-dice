package io.hmpl.boundary.rest.resource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import io.hmpl.control.series.SeriesService;
import java.net.HttpURLConnection;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("series")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class SeriesResource {

	@Inject
	private SeriesService seriesService;

	@GET
	public Response getSeries() {
		return seriesService.getSeries()
				.map(s -> Response.ok().entity(s))
				.orElseGet(() -> Response.ok())
				.build();
	}

	@GET
	@Path("{name}")
	public Response getSeries(@PathParam("name") final String name) {
		return seriesService.getSeries(name)
				.map(s -> Response.ok().entity(s))
				.orElseGet(() -> Response.status(HttpURLConnection.HTTP_NOT_FOUND))
				.build();
	}

}
