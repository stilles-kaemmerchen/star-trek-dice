package io.hmpl.boundary.rest.resource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import io.hmpl.boundary.rest.response.SlackResponse;
import io.hmpl.control.EpisodeService;
import io.hmpl.persistence.jpa.entity.Episode;
import io.vavr.control.Either;
import java.net.HttpURLConnection;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("episodes")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class EpisodeResource {

	@Inject
	private EpisodeService episodeService;

	@GET
	@Path("random")
	public Response getRandom() {
		Optional<Episode> episodeOptional = episodeService.getRandom();

		if(episodeOptional.isPresent()) {
			Episode episode = episodeOptional.get();
			SlackResponse slackResponse = new SlackResponse(episode);
			return Response.ok().entity(slackResponse).build();
		} else {
			return Response.status(HttpURLConnection.HTTP_NOT_FOUND).build();
		}
	}
}
