package io.hmpl.boundary.rest.response;

import io.hmpl.persistence.jpa.entity.Episode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SlackResponse {

	public final String response_type = "in_channel";
	public List<SlackAttachment> attachments = new ArrayList<>();

	public SlackResponse() {}

	public SlackResponse(Episode episode) {
		SlackAttachment attachment = new SlackAttachment(episode);
		attachments.add(attachment);
	}

	public class SlackAttachment {
		public String title;
		public String title_link;
		public String text;
		public String footer = "Memory Alpha";
		public final Date ts = new Date();
		public final String footer_icon = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Wikipedia%27s_W.svg/200px-Wikipedia%27s_W.svg.png";

		private Episode episode;

		public SlackAttachment(Episode episode) {
			this.title = String.format("%s(S%02dE%02d %s)", episode.getName(), episode.getSeason(), episode.getNumber(), episode.getSeries().getName());
			this.title_link = episode.getUrl();
			this.text = episode.getTeaser();
		}
	}
}
