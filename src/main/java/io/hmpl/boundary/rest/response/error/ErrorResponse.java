package io.hmpl.boundary.rest.response.error;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

public class ErrorResponse {
	@ApiModelProperty(notes = "the errors")
	private final List<DetailedError> errors;

	@JsonCreator
	public ErrorResponse(
			@JsonProperty("errors")
			final List<DetailedError> errors
	) {
		this.errors = errors;
	}

	public List<DetailedError> getErrors() {
		return errors;
	}
}
