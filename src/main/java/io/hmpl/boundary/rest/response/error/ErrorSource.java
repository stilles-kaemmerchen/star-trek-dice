package io.hmpl.boundary.rest.response.error;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class ErrorSource {
	@ApiModelProperty(notes = "a pointer to indicate where the error occured")
	private final String pointer;

	@JsonCreator
	public ErrorSource(
			@JsonProperty("pointer") final String pointer
	) {
		this.pointer = pointer;
	}

	public String getPointer() {
		return pointer;
	}
}
