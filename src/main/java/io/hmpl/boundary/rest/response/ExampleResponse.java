package io.hmpl.boundary.rest.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;


public class ExampleResponse {
	@ApiModelProperty(notes = "the data")
	private final List<String> data;

	@JsonCreator
	public ExampleResponse(
			@JsonProperty("data") final List<String> data
	) {
		this.data = data;
	}

	public List<String> getData() {
		return data;
	}
}
