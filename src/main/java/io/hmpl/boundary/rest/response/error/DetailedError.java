package io.hmpl.boundary.rest.response.error;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class DetailedError {
	@ApiModelProperty(notes = "the status code")
	private final int status;

	@ApiModelProperty(notes = "the title of the error")
	private final String title;

	@ApiModelProperty(notes = "the detailed description of the error")
	private final String detail;

	@ApiModelProperty(notes = "a more detailed info where the error occured")
	private final ErrorSource source;

	@JsonCreator
	public DetailedError(
			@JsonProperty("status") final int status,
			@JsonProperty("title") final String title,
			@JsonProperty("detail") final String detail,
			@JsonProperty("source") final ErrorSource source
	) {
		this.status = status;
		this.title = title;
		this.detail = detail;
		this.source = source;
	}

	public int getStatus() {
		return status;
	}

	public String getTitle() {
		return title;
	}

	public String getDetail() {
		return detail;
	}

	public ErrorSource getSource() {
		return source;
	}
}
