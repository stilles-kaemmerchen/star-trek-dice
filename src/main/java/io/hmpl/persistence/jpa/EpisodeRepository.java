package io.hmpl.persistence.jpa;

import io.hmpl.persistence.jpa.entity.Episode;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EpisodeRepository {

	@PersistenceContext(unitName = "startrek")
	private EntityManager entityManager;

	public List<Episode> getEpisodes() {
		return entityManager.createQuery("SELECT e FROM Episode e", Episode.class).getResultList();
	}

	public Episode getRandomEpisode() {
		List<Episode> episodes =
				entityManager.createQuery("SELECT e FROM Episode e", Episode.class).getResultList();

		Collections.shuffle(episodes);

		return episodes.get(0);
	}
}
