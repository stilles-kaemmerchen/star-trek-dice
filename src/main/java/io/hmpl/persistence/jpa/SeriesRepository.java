package io.hmpl.persistence.jpa;

import io.hmpl.persistence.jpa.entity.Series;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class SeriesRepository {

	@PersistenceContext(unitName = "startrek")
	private EntityManager entityManager;

	public List<Series> getSeries() {
		return entityManager.createQuery("SELECT s from Series s", Series.class).getResultList();
	}

	public Series getSeries(final String name) {
		return entityManager.find(Series.class, name);
	}
}
