package io.hmpl.persistence.jpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class Episode {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer season;
	private Integer number;
	private String name;
	private String slug;
	private String url;
	@Lob
	private String teaser;

	@OneToOne(cascade={CascadeType.ALL})
	private Series series;

	public Integer getSeason() {
		return season;
	}

	public void setSeason(Integer season) {
		this.season = season;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Series getSeries() {
		return series;
	}

	public void setSeries(Series series) {
		this.series = series;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTeaser() {
		return teaser;
	}

	public void setTeaser(String teaser) {
		this.teaser = teaser;
	}

	@Override
	public String toString() {
		return String.format(
				"Episode {id=%s, seasonCode=%s name=%s}",
				this.id,
				this.season + "x" + this.number,
				this.name
		);
	}
}
