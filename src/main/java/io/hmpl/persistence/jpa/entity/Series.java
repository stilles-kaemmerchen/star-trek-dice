package io.hmpl.persistence.jpa.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Series {

	@Id
	private String id;
	private String name;

	@OneToMany
	private List<Episode> episodes = Collections.emptyList();

	protected Series() {}

	public Series(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public Series(String id, String name, List<Episode> episodes) {
		this.id = id;
		this.name = name;
		this.episodes = episodes;
		this.episodes = Collections.emptyList();
	}

	public Series(String name, List<Episode> episodes) {
		this.name = name;
		this.episodes = episodes;
		this.episodes = Collections.emptyList();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}
}
