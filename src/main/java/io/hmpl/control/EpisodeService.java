package io.hmpl.control;

import io.hmpl.persistence.jpa.entity.Episode;
import java.util.List;
import java.util.Optional;

public interface EpisodeService {

	Optional<List<Episode>> getEpisodes(final String name);

	Optional<Episode> getRandom();
}
