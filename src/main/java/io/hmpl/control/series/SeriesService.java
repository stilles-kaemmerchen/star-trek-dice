package io.hmpl.control.series;

import io.hmpl.persistence.jpa.entity.Series;
import java.util.List;
import java.util.Optional;

public interface SeriesService {

	Optional<List<Series>> getSeries();

	Optional<Series> getSeries(final String name);
}
