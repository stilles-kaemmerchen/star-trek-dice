package io.hmpl.control.series;

import io.hmpl.persistence.jpa.SeriesRepository;
import io.hmpl.persistence.jpa.entity.Series;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

public class SeriesServiceImpl implements SeriesService {

	@Inject
	private SeriesRepository seriesRepository;

	@Override
	public Optional<List<Series>> getSeries() {
		return Optional.of(seriesRepository.getSeries());
	}

	@Override public Optional<Series> getSeries(final String name) {
		Series series = seriesRepository.getSeries(name);

		if(series == null) {
			return Optional.empty();
		}

		return Optional.of(series);
	}
}
