package io.hmpl.control;

import io.hmpl.persistence.jpa.EpisodeRepository;
import io.hmpl.persistence.jpa.entity.Episode;
import io.vavr.control.Either;
import io.vavr.control.Option;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

public class EpisodeserviceImpl implements EpisodeService {

	@Inject
	private EpisodeRepository episodeRepository;

	@Inject
	private EpisodeDescriptionService episodeDescriptionService;

	@Override
	public Optional<List<Episode>> getEpisodes(String name) {
		return Optional.of(episodeRepository.getEpisodes());
	}

	@Override
	public Optional<Episode> getRandom() {

		Episode randomEpisode = episodeRepository.getRandomEpisode();

		return Optional.of(randomEpisode);
	}
}
