package io.hmpl.control;

import io.hmpl.persistence.jpa.entity.Episode;
import io.vavr.control.Either;

public interface EpisodeDescriptionService {

	Either<Throwable, String> fetchEpisodeDescription(Episode episode);
}
