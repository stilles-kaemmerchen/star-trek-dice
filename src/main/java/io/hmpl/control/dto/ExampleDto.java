package io.hmpl.control.dto;

import java.util.List;

public class ExampleDto {
	private final List<String> data;

	public ExampleDto(final List<String> data) {
		this.data = data;
	}

	public List<String> getData() {
		return data;
	}
}
