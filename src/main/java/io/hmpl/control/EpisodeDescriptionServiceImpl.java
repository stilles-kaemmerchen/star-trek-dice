package io.hmpl.control;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import io.hmpl.persistence.jpa.entity.Episode;
import io.vavr.control.Either;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class EpisodeDescriptionServiceImpl implements EpisodeDescriptionService {

	private final String wikipediaUrl = "https://en.wikipedia.org/w/api.php";

	@Override
	public Either<Throwable, String> fetchEpisodeDescription(Episode episode) {
		try {
			HttpResponse<JsonNode> jsonNodeHttpResponse =
					Unirest.get(wikipediaUrl)
							.queryString("format", "json")
							.queryString("action", "parse")
							.queryString("page", episode.getName())
							.asJson();

			String text =
					jsonNodeHttpResponse.getBody().getObject().getJSONObject("parse")
							.getJSONObject("text").getString("*");

			Document parse = Jsoup.parse(text);
			Element teaser = parse.getElementById("Teaser");
			String teaserContent = teaser.parent().nextElementSibling().text();

			return Either.right(jsonNodeHttpResponse.getBody().toString());
		} catch (Exception e) {
			return Either.left(e);
		}
	}
}
