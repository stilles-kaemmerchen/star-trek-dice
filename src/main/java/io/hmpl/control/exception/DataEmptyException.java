package io.hmpl.control.exception;

public class DataEmptyException extends RuntimeException {
	public DataEmptyException() {
		super();
	}

	public DataEmptyException(final String message) {
		super(message);
	}

	public DataEmptyException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DataEmptyException(final Throwable cause) {
		super(cause);
	}

	protected DataEmptyException(
			final String message,
			final Throwable cause,
			final boolean enableSuppression,
			final boolean writableStackTrace
	) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
